package no.uib.inf101.gridview;

import java.awt.Color;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    // Opprett et rutenett med 3 rader og 4 kolonner
    IColorGrid grid = new ColorGrid(3, 4);

    // Sjekk at vi kan endre verdien på en gitt posisjon        
    grid.set(new CellPosition(0, 0), Color.RED);
    grid.set(new CellPosition(0, 3), Color.BLUE);
    grid.set(new CellPosition(2, 0), Color.YELLOW);
    grid.set(new CellPosition(2, 3), Color.GREEN);
    System.out.println(grid.get(new CellPosition(0, 0))); // forventer rød
    System.out.println(grid.get(new CellPosition(0, 3)));
    System.out.println(grid.get(new CellPosition(2, 0)));
    System.out.println(grid.get(new CellPosition(2, 3)));
    


    // Grid view
    
    GridView canvas = new GridView(grid);
    JFrame frame = new JFrame();
    frame.setTitle("INF101");
    frame.setContentPane(canvas);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
