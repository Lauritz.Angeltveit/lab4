package no.uib.inf101.gridview;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.List;


public class GridView extends JPanel{
  int width = 400;
  int height = 300;
  IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private static final double gridMargin = 30;
  
  public GridView(IColorGrid colorGrid) {
    this.setPreferredSize(new Dimension(width, height));
    this.colorGrid = colorGrid;
  }
  
  

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
    
    
  }

  private void drawGrid(Graphics2D graphics2d) {
      Rectangle2D greyArea = new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, getWidth() -2*OUTERMARGIN, getHeight() - 2*OUTERMARGIN);
      graphics2d.setColor(MARGINCOLOR);
      graphics2d.fill(greyArea);
      CellPositionToPixelConverter cellPixels = new CellPositionToPixelConverter(greyArea, colorGrid, gridMargin);
      drawCells(graphics2d, colorGrid, cellPixels);
  }   

  private static void drawCells(Graphics2D graphics2d, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPositionToPixelConverter) {
      List<CellColor> cells = cellColorCollection.getCells();
      for (CellColor cell : cells) {
        Rectangle2D rectangle = cellPositionToPixelConverter.getBoundsForCell(cell.cellPosition());
        Color color = cell.color();
        if (color == null) {
        color = Color.DARK_GRAY;
      }
      graphics2d.setColor(color);
      graphics2d.fill(rectangle);
    }
  }

}
